using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Context;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class ThreadDataLoader : IDataLoader
    {
        private List<Customer> _bigData;
        private int _countThreads;
        public ThreadDataLoader(int countThreads)
        {
            _countThreads = countThreads;
        }
        public void LoadData(List<Customer> bigData)
        { 
            //лучшее время 2:10 
            //убрал асинхронность и время улучшилось 2:06 (блокировки не получал)
            var countThreads = _countThreads;
            var data = bigData;
            for (int i = 0; i < countThreads; i++)
            {
                var takeItems = data.Count() / countThreads; 
                var j = i;
                var thread = new Thread(() => Load(data.Skip(j * takeItems).Take(takeItems).ToList()))
                {
                    IsBackground = false
                };
                thread.Name = $"{thread}{j.ToString()}";
                thread.Start();
            }
        }

        public Task LoadDataAsync(List<Customer> data)
        {
            throw new NotImplementedException();
        }

        private void Load(List<Customer> data)
        {
            Console.WriteLine($"Thread={Thread.CurrentThread.Name} START load Time={DateTime.Now.ToString(CultureInfo.InvariantCulture)}");
            using var context = new DataContext();
            var batchLimit = 25000;
            var countIteration = IDataLoader.GetCountIteration(data.Count(), batchLimit);
            for (int x = 0; x < countIteration; x++)
            {
                SaveToDbRange(context, data.Skip(x * batchLimit).Take(batchLimit).ToList());
            }
            Console.WriteLine($"Thread={Thread.CurrentThread.Name} END load Time={DateTime.Now.ToString(CultureInfo.InvariantCulture)}");
        }
        
        private void SaveToDbRange(DataContext context, List<Customer> customers)
        {
            Console.WriteLine($"Thread={Thread.CurrentThread.Name} START SaveToDbRange Time={DateTime.Now.ToString(CultureInfo.InvariantCulture)}");
            var repository = new CustomerRepository(context);
            repository.AddCustomers(customers.ToList());
            repository.SaveWithAttempt();
            Console.WriteLine($"Thread={Thread.CurrentThread.Name} END SaveToDbRange Time={DateTime.Now.ToString(CultureInfo.InvariantCulture)}");
        }
    }
}