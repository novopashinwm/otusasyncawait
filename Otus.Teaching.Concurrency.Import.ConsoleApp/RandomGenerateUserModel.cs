using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Models;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;

namespace Otus.Teaching.Concurrency.Import.ConsoleApp
{
    public class RandomGenerateUserModel
    {
        public async Task<UserModel> GenerateAsync()
        {
            var customers = await RandomCustomerGenerator.GenerateAsync(1);
            var customer = customers.FirstOrDefault();
            return new UserModel
            {
                Id = customer.Id,
                Email = customer.Email,
                Phone = customer.Phone,
                FullName = customer.FullName
            };
        }
    }
}