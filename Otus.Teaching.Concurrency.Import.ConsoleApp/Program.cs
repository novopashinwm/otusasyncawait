﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Otus.Teaching.Concurrency.Import.Core.Models;

namespace Otus.Teaching.Concurrency.Import.ConsoleApp
{
    class Program
    {
        private static string CREATE = "-crt";
        private static string EXIT = "-ext";
        
        static async Task Main(string[] args)
        {
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => true;
            var httpClient = new HttpClient(clientHandler);
            var sw = new Stopwatch();
            while (true)
            {
                var randomNumber = new Random();
                
                Console.WriteLine($"Введите id пользователя, {CREATE} или {EXIT}");
                var message = Console.ReadLine();
                var infoUserModel = string.Empty;
                if (string.IsNullOrEmpty(message)) continue;
                if (int.TryParse(message, out var id))
                {
                    sw.Start();
                    var response = await httpClient.GetAsync($"http://localhost:5000/users/{id}");
                    var jsonString = await response.Content.ReadAsStringAsync();
                    var obj = JsonConvert.DeserializeObject<UserModel>(jsonString);
                    infoUserModel = ShowInfo(obj, ((int)response.StatusCode).ToString());
                    sw.Stop();
                    Console.WriteLine($"Запрос выполнился за {sw.ElapsedMilliseconds} мс");
                    sw.Reset();
                } else if (message.Equals(CREATE))
                {
                    var random = new RandomGenerateUserModel();
                    var userModel = await random.GenerateAsync();
                    userModel.Id = randomNumber.Next(1, 1500000);
                    Console.WriteLine($"Try to add пользователя с Id = {userModel.Id}");
                    var json = JsonConvert.SerializeObject(userModel);
                    var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = await httpClient.PostAsync($"http://localhost:5000/users", httpContent);
                    var jsonString = await response.Content.ReadAsStringAsync();
                    var obj = JsonConvert.DeserializeObject<UserModel>(jsonString);
                    infoUserModel = ShowInfo(obj, ((int)response.StatusCode).ToString());
                } else if (message.Equals(EXIT))
                {
                    return;
                } 
                Console.WriteLine(infoUserModel);
                Console.WriteLine("---------------------------------------------------------");
            }
        }

        static string ShowInfo(UserModel obj, string statusCode)
        {
            var sb = new StringBuilder();
            sb.AppendLine($"StatusCode = {statusCode}");
            sb.AppendLine($"Id = {obj.Id}");
            sb.AppendLine($"Name = {obj.FullName}");
            sb.AppendLine($"Phone = {obj.Phone}");
            sb.AppendLine($"Email = {obj.Email}");
            return sb.ToString();
        }
    }
}