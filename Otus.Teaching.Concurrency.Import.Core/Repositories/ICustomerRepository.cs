using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
    public interface ICustomerRepository
    {
        Customer Get(int id);
        Task<Customer> GetAsync(int id);
        Customer AddCustomer(Customer customer);
        Task<Customer> AddCustomerAsync(Customer customer);
        void AddCustomers(List<Customer> customers);
        Task AddCustomersAsync(List<Customer> customers);
        void RemoveCustomer(Customer customer);
        Task<int> GetCustomersCountAsync();
        void RemoveSpecifyCustomers(IEnumerable<int> ids);
        void Save();
        Task SaveAsync();
    }
}