﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public interface IDataLoader
    {
        void LoadData(List<Customer> data);
        Task LoadDataAsync(List<Customer> data);
        static int GetCountIteration(int countItems, int batchLimit)
        {
            return countItems / batchLimit + (countItems % batchLimit != 0 ? 1 : 0);
        }
    }
}