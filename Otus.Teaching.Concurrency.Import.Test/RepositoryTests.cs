using System.Threading.Tasks;
using NUnit.Framework;
using Otus.Teaching.Concurrency.Import.DataAccess.Context;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Test
{
    public class RepositoryTests
    {
        [Test]
        public async Task AddCustomer()
        {
            var context = new DataContext();
            var repository = new CustomerRepository(context);
            var existingCustomer = await repository.GetAsync(-1);
            if (existingCustomer != null)
            {
                repository.RemoveCustomer(existingCustomer);
            }
            var customer = new Customer
            {
                Id = -1
            };
            var addedCustomer = await repository.AddCustomerAsync(customer);
            Assert.NotNull(addedCustomer, "Количество добавленных элементов is null");
        }
    }
}