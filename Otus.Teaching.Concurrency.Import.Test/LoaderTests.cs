using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Context;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;

namespace Otus.Teaching.Concurrency.Import.Test
{
    public class LoaderTests
    {
        [Test]
        public async Task LoadCustomer()
        {
            var random = new Random();
            var countCustomersForGenerate = random.Next(1, 10);
            var context = new DataContext();
            var repository = new CustomerRepository(context);
            var countBefore = await repository.GetCustomersCountAsync();
            
            var customers = await RandomCustomerGenerator.GenerateAsync(countCustomersForGenerate);
            var specifyCustomers = new List<int>();
            for (int i = -1; i >= -countCustomersForGenerate; i--)
            {
                customers[-i - 1].Id = i;
                specifyCustomers.Add(i);
            }
            var loader = new TaskDataLoader(1);
            await loader.LoadDataAsync(customers);
            Thread.Sleep(TimeSpan.FromSeconds(1));
            var countAfter = await repository.GetCustomersCountAsync();
            repository.RemoveSpecifyCustomers(specifyCustomers);
            Assert.AreEqual(countAfter, countBefore + countCustomersForGenerate, "Количество элементов после добавления элементов не совпадает с ожидаемым!");
        }
    }
}