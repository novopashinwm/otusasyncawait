using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using ServiceStack.Text;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CsvGenerator : IDataGenerator
    {
        private readonly string _fileName;
        private readonly int _dataCount;

        public CsvGenerator(string fileName, int dataCount)
        {
            _fileName = fileName;
            _dataCount = dataCount;
        }
        
        public void Generate()
        {
            Write(RandomCustomerGenerator.Generate(_dataCount));
        }

        public async Task GenerateAsync()
        {
            Write(await RandomCustomerGenerator.GenerateAsync(_dataCount));
        }

        private void Write(List<Customer> customers)
        {
            using var stream = File.Create(_fileName);
            stream.WriteCsv(customers);
        }
    }
}