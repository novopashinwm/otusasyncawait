﻿using System;
using System.IO;
using System.Threading.Tasks;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    class Program
    {
        private static readonly string _dataFileDirectory = AppDomain.CurrentDomain.BaseDirectory;
        private static string _dataFileName; 
        private static int _dataCount = 100; 
        
        static async Task Main(string[] args)
        {
            if (!TryValidateAndParseArgs(args))
                return;
            
            Console.WriteLine("Generating data...");

            var generator = GeneratorFactory.GetCsvGenerator(_dataFileName, _dataCount);
            
            await generator.GenerateAsync();
            
            Console.WriteLine($"Generated data in {_dataFileName}...");
        }

        private static bool TryValidateAndParseArgs(string[] args)
        {
            if (args != null && args.Length > 0)
            {
                _dataFileName = Path.Combine(_dataFileDirectory, $"{args[0]}");
            }
            else
            {
                Console.WriteLine("Data file name without extension is required");
                return false;
            }
            
            if (args.Length > 1)
            {
                if (!int.TryParse(args[1], out _dataCount))
                {
                    Console.WriteLine("Data must be integer");
                    return false;
                }
            }

            return true;
        }
    }
}