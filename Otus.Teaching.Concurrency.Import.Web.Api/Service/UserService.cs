using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Models;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.Web.Api.Service
{
    public class UserService : IUserService
    {
        private ICustomerRepository _repository;
        public UserService(ICustomerRepository repository)
        {
            _repository = repository;
        }

        public async Task<UserModel> GetUserAsync(int id)
        {
            var entity = await _repository.GetAsync(id);
            if (entity == null)
                return null;
            var model = UserModel.ConvertFromEntity(entity);
            return model;
        }

        public async Task<UserModel> AddUserAsync(UserModel model)
        {
            var entity = UserModel.ConvertToEntity(model);
            var addedEntity = await _repository.AddCustomerAsync(entity);
            return addedEntity == null ? null : UserModel.ConvertFromEntity(addedEntity);
        }
    }
}