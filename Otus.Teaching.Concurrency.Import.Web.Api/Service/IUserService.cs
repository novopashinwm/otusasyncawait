using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Models;

namespace Otus.Teaching.Concurrency.Import.Web.Api.Service
{
    public interface IUserService
    {
        Task<UserModel> GetUserAsync(int id);
        Task<UserModel> AddUserAsync(UserModel model);
    }
}