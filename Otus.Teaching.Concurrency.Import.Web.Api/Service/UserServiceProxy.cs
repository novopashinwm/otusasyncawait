using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core.Models;

namespace Otus.Teaching.Concurrency.Import.Web.Api.Service
{
    public class UserServiceProxy : IUserService
    {
        private IUserService _userService;
        public UserServiceProxy(IUserService userService)
        {
            _userService = userService;
        }
        
        public async Task<UserModel> GetUserAsync(int id)
        {
            var userKeyValue = Cache.UserModelDict.FirstOrDefault(x => x.Key == id);
            if (userKeyValue.Value != null)
            {
                userKeyValue.Value.CountCalls++;
                return userKeyValue.Value.UserModel;
            }
            var user = await _userService.GetUserAsync(id);
            Cache.UserModelDict.Add(id, new CacheItem { CountCalls = 1, UserModel = user});
            return user;
        }

        public async Task<UserModel> AddUserAsync(UserModel model)
        {
            return await _userService.AddUserAsync(model);
        }
    }
}