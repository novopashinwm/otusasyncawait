using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Core.Models;

namespace Otus.Teaching.Concurrency.Import.Web.Api.Service
{
    public static class Cache
    {
        public static List<UserModel> UserModel { get; set; } = new List<UserModel>();
        public static Dictionary<int, CacheItem> UserModelDict { get; set; } = new Dictionary<int, CacheItem>();
    }

    public class CacheItem
    {
        public UserModel UserModel { get; set; }
        public int CountCalls { get; set; }
    }
}