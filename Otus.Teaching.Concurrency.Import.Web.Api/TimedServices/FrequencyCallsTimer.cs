using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Web.Api.Service;

namespace Otus.Teaching.Concurrency.Import.Web.Api.TimedServices
{
    public class FrequencyCallsTimer : TimedHostedService
    {
        public FrequencyCallsTimer()
        {
            Timer = new Timer(DoWork, null, TimeSpan.Zero, 
                TimeSpan.FromSeconds(10)); // TODO 10 секунд для быстрой проверки
        }
        
        protected override void DoWork(object state)
        {
            if (!Cache.UserModelDict.Any()) return;
            var averageFrequency = Cache.UserModelDict.Values.Average(x => x.CountCalls);
            var idsForRemove = new List<int>();
            foreach (var cacheItem in Cache.UserModelDict)
            {
                if (cacheItem.Value.CountCalls < averageFrequency)
                {
                    idsForRemove.Add(cacheItem.Key);
                }
            }
            foreach (var idRemove in idsForRemove)
            {
                Cache.UserModelDict.Remove(idRemove);
            }
        }
    }
}