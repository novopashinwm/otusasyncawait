using System;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public static class Extensions
    {
        public static void SaveWithAttempt(this ICustomerRepository repository, int attempt = 5)
        {
            try
            {
                repository.Save();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Попали в Exception для потока {Thread.CurrentThread.Name}");
                if (attempt > 0)
                {
                    --attempt;
                    ShowingAttempts(ex, attempt);
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                    SaveWithAttempt(repository, attempt);
                }
                Console.WriteLine($"Попыток не осталось для потока {Thread.CurrentThread.Name}");
            }
        }
        
        public static async Task SaveWithAttemptAsync(this ICustomerRepository repository, int attempt = 5)
        {
            try
            {
                await repository.SaveAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Попали в Exception для потока {Thread.CurrentThread.Name}");
                if (attempt > 0)
                {
                    --attempt;
                    ShowingAttempts(ex, attempt);
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                    await SaveWithAttemptAsync(repository, attempt);
                }
                Console.WriteLine($"Попыток не осталось для потока {Thread.CurrentThread.Name}");
            }
        }
        
        private static void ShowingAttempts(Exception ex, int attempt)
        {
            Console.WriteLine($"Число попыток сохранения в потоке осталось {Thread.CurrentThread.Name} равно {attempt}. Exception: {ex?.Message}. InnerException: {ex.InnerException?.Message}");
        }
    }
}