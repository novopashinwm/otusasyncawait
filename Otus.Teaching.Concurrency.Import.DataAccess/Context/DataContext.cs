using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Context
{
    public class DataContext: DbContext
    {
        private string path = Path.Combine(Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.Parent.Parent.FullName, @"Otus.Teaching.Concurrency.Import.DataAccess/data.db");
        public DataContext(DbContextOptions<DataContext> options): base(options){}
        public DataContext(){}
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($@"Data Source={path}");
        }
        public DbSet<Customer> Customers { get; set; }
    }
}